package id.ishom.draw_lib.constant

internal class ActionMode {
    companion object {
        var NONE = 0
        var DRAG = 1
        var ZOOM_WITH_TWO_FINGER = 2
        var ICON = 3
        var CLICK = 4
    }
}