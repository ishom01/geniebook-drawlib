package id.ishom.draw_lib.constant

import android.content.res.Resources

internal class ConstantShapeIcon {
    companion object{
        val DEFAULT_ICON_RADIUS = 12f * Resources.getSystem().displayMetrics.density
        val DEFAULT_ROTATION_HEIGHT = 32f * Resources.getSystem().displayMetrics.density

        const val LEFT_TOP = 0
        const val RIGHT_TOP = 1
        const val LEFT_BOTTOM = 2
        const val RIGHT_BOTTOM = 3
        const val ROTATION = 4
    }
}