package id.ishom.draw_lib.constant

internal class ConstantShape {
    companion object{
        var CENTER = 1
        var TOP = 1 shl 1
        var LEFT = 1 shl 2
        var RIGHT = 1 shl 3
        var BOTTOM = 1 shl 4
    }
}