package id.ishom.draw_lib.constant

enum class DrawMode() {
    NULL,
    LINE,
    SHAPE
}
