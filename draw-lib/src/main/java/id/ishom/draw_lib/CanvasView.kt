package id.ishom.draw_lib

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.core.content.ContextCompat
import id.ishom.draw_lib.model.view.PaintView
import id.ishom.draw_lib.constant.DrawMode
import id.ishom.draw_lib.listener.CanvasEditorListener
import id.ishom.draw_lib.listener.FrameViewListener
import id.ishom.draw_lib.listener.PaintViewListener
import id.ishom.draw_lib.model.DrawObject
import id.ishom.draw_lib.model.shape.DrawableShape
import id.ishom.draw_lib.model.view.FrameView

class CanvasView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : RelativeLayout(context, attrs) {

    private val mUndoList = mutableListOf<DrawObject>()
    private val mRedoList = mutableListOf<DrawObject>()

    private val paintViewListener = object : PaintViewListener {
        override fun onTouchUp(obj: DrawObject) {
            Log.e("Canvas finish draw", "${obj.shape?.height}, ${obj.shape?.width}")
            mUndoList.add(obj)
            mRedoList.clear()
            mListener?.onEnableUndo(true)
            mListener?.onEnableRedo(false)
        }

        override fun onClick(x: Float, y: Float) {
            Log.e("Canvas Debug", "click pos $x, $y")
            val pos = findTapedSticker(x, y)
            if(pos > -1)
                enableEditModeSticker(pos)
        }

        override fun onTouchEvent(event: MotionEvent) {
            mListener?.onTouchEvent(event)
        }

        override fun onFinishDrawObj(obj: DrawObject) {
            mUndoList.add(obj)
            mRedoList.clear()
            mListener?.onEnableUndo(true)
            mListener?.onEnableRedo(false)
        }
    }

    private val frameViewListener = object : FrameViewListener {
        override fun onRemove() {
            mListener?.onShapeRemove()
            mListener?.onEnableUndo(mUndoList.isNotEmpty())
        }
        override fun onDone(obj: DrawObject) {
            addStickerToPaint(obj)
            mListener?.onShapeDone()
        }
        override fun onZoomAndRotate() {
            mListener?.onShapeZoomAndRotate()
        }

        override fun onShapeResize() {
            mListener?.onShapeResize()
        }

        override fun onFlip() {
            mListener?.onShapeFlip()
        }

        override fun onClickStickerOutside(x: Float, y: Float) {
            val pos = findTapedSticker(x, y)
            if(pos > -1){
                enableEditModeSticker(pos)
            }
        }

        override fun onTouchEvent(event: MotionEvent) {
            mListener?.onTouchEvent(event)
        }
    }

    private val mPaintView: PaintView = PaintView(context, paintViewListener)
    private val mFrameView = FrameView(context, frameViewListener)
    private var mListener: CanvasEditorListener? = null

    var drawMode = DrawMode.NULL
        set(value) {
            field = value
            mPaintView.setDrawMode(field)
        }

    init {
        val params = LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT
        )
        mPaintView.layoutParams = params
        mPaintView.setBackgroundColor(ContextCompat.getColor(context, android.R.color.white))
        addView(mPaintView)

        mFrameView.layoutParams = params
        mFrameView.setBackgroundColor(
            ContextCompat.getColor(
                context,
                android.R.color.transparent
            )
        )
        addView(mFrameView)
        mFrameView.visibility = View.GONE
    }

    fun setListener(listener: CanvasEditorListener) {
        mListener = listener
    }

    fun setPaintColor(color: Int) {
        doneStickerEdit()
        mPaintView.paint.color = color
    }

    fun setStrokeWidth(strokeWidth: Float) {
        doneStickerEdit()
        mPaintView.paint.strokeWidth = strokeWidth
    }

    //region add shape
    fun addDrawableSticker(drawable: Drawable) {
        doneStickerEdit()
        mFrameView.visibility = View.VISIBLE
        val shape = DrawableShape(drawable)
        mFrameView.addSticker(shape)
        mListener?.onEnableUndo(true)
        mListener?.onEnableRedo(false)
        mListener?.onShapeActive()
    }

    fun doneActiveSticker(){
        if (mFrameView.visibility == View.VISIBLE) {
            mFrameView.done()
        }
    }

    fun removeActiveSticker(){
        if (mFrameView.visibility == View.VISIBLE) {
            mFrameView.remove()
        }
    }

    fun zoomAndRotateActiveSticker(motionEvent: MotionEvent){
        if (mFrameView.visibility == View.VISIBLE) {
            mFrameView.zoomAndRotate(motionEvent)
        }
    }

    fun flipActiveSticker(){
        if (mFrameView.visibility == View.VISIBLE) {
            mFrameView.flip()
        }
    }
    //endregion

    fun undo() {
        if (mFrameView.visibility == View.VISIBLE) {
            mFrameView.remove()
            return
        }
        if (mUndoList.isNotEmpty()) {
            mRedoList.add(mUndoList.last())
            mUndoList.removeAt(mUndoList.lastIndex)
            mPaintView.initCanvas()
            mUndoList.forEach {
                drawObject(it)
            }
            mListener?.onEnableUndo(mUndoList.isNotEmpty())
            mListener?.onEnableRedo(mRedoList.isNotEmpty())
        }
    }

    fun redo() {
        if (mRedoList.isNotEmpty()) {
            val obj = mRedoList.last()
            mUndoList.add(obj)
            mRedoList.removeAt(mRedoList.lastIndex)
            drawObject(obj)
            mListener?.onEnableUndo(mUndoList.isNotEmpty())
            mListener?.onEnableRedo(mRedoList.isNotEmpty())
        }
    }

    fun removeAll(){
        mUndoList.clear()
        mRedoList.clear()
        mFrameView.remove()
        mPaintView.initCanvas()
        mListener?.onEnableUndo(false)
        mListener?.onEnableRedo(false)
    }

    fun downloadBitmap(): Bitmap{
        doneStickerEdit()
        return mPaintView.extraBitmap
    }

    private fun drawObject(obj: DrawObject) {
        when (obj.drawMode) {
            DrawMode.LINE -> {
                mPaintView.drawPath(obj.pathAndPaint!!)
            }
            DrawMode.SHAPE -> {
                mPaintView.drawSticker(obj.shape!!)
            }
        }
    }

    //region find double tap inside shape
    private fun findTapedSticker(x: Float, y: Float): Int {
        Log.e("Canvas Debug", "find undo size ${mUndoList.size}")
        for (i in mUndoList.size - 1 downTo 0) {
            val obj = mUndoList[i]
            if (obj.drawMode == DrawMode.SHAPE) {
                val shape = obj.shape!!
                Log.e("Canvas Debug", "$i ${shape.width} ${shape.height} ${shape.contains(x, y)}")
                if (shape.contains(x, y)) {
                    return i
                }
            }
        }
        return -1
    }

    private fun enableEditModeSticker(pos: Int) {
        val obj = mUndoList[pos]
        val shape = obj.shape!!
        mFrameView.visibility = View.VISIBLE
        mFrameView.currentShape = shape
        mUndoList.removeAt(pos)
        mPaintView.initCanvas()
        mUndoList.forEach {
            drawObject(it)
        }
        mRedoList.clear()
        mListener?.onEnableUndo(true)
        mListener?.onEnableRedo(mRedoList.isNotEmpty())
        mListener?.onShapeActive()
    }

    private fun addStickerToPaint(obj: DrawObject) {
        mPaintView.drawSticker(obj.shape!!)
        mUndoList.add(obj)
        mRedoList.clear()
        mListener?.onEnableUndo(true)
        mListener?.onEnableRedo(false)
    }

    private fun doneStickerEdit() {
        if (mFrameView.visibility == View.VISIBLE) {
            mFrameView.done()
        }
    }
}
//endregion