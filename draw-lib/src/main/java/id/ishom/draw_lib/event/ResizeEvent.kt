package id.ishom.draw_lib.event

import android.view.MotionEvent
import id.ishom.draw_lib.listener.IconListener
import id.ishom.draw_lib.model.view.FrameView

internal class ResizeEvent: IconListener {
    override fun onActionDown(frameView: FrameView?, event: MotionEvent?) {}
    override fun onActionMove(frameView: FrameView, event: MotionEvent) {
        frameView.resize(event)
    }
    override fun onActionUp(frameView: FrameView, event: MotionEvent?) {}
}