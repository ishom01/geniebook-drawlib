package id.ishom.draw_lib.model.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.view.MotionEvent
import android.view.View
import android.view.ViewConfiguration
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.ViewCompat
import id.ishom.draw_lib.R
import id.ishom.draw_lib.calculateDistanceRotation
import id.ishom.draw_lib.constant.*
import id.ishom.draw_lib.event.*
import id.ishom.draw_lib.listener.FrameViewListener
import id.ishom.draw_lib.model.DrawObject
import id.ishom.draw_lib.model.shape.BaseShape
import id.ishom.draw_lib.model.shape.Icon
import java.util.*
import kotlin.math.abs
import kotlin.math.atan2
import kotlin.math.pow
import kotlin.math.sqrt

internal class FrameView (context: Context, private val frameViewListener: FrameViewListener) :
    FrameLayout(context) {
    var currentShape: BaseShape? = null

    private var currentMode = ActionMode.NONE

    private var isTouchInsideSticker = false

    private val stickerRect = RectF()
    private val icons: MutableList<Icon> = ArrayList(4)
    private val bitmapPoints = FloatArray(8)
    private val bounds = FloatArray(8)
    private val point = FloatArray(2)
    private val currentCenterPoint = PointF()
    private val tmp = FloatArray(2)
    private var midPoint = PointF()

    private val sizeMatrix = Matrix()
    private val downMatrix = Matrix()
    private val moveMatrix = Matrix()

    private var downX = 0f
    private var downY = 0f
    private var oldDistance = 0f
    private var oldRotation = 0f
    private val drawColor = ResourcesCompat.getColor(resources, android.R.color.black, null)

    val paint = Paint().apply {
        color = drawColor
        style = Paint.Style.STROKE
        strokeJoin = Paint.Join.ROUND
        strokeCap = Paint.Cap.ROUND
        strokeWidth = 10f
    }

    private val borderPaint = Paint().apply {
        isAntiAlias = true
        color = Color.BLACK
        alpha = 50
    }
    private val iconPaint = Paint().apply {
        isAntiAlias = true
        color = Color.TRANSPARENT
    }

    private val touchSlop: Int = ViewConfiguration.get(context).scaledTouchSlop
    private var currentIcon: Icon? = null

    init {
        configDefaultIcons()
    }

    private fun configDefaultIcons() {
        val deleteIcon = Icon(
            ContextCompat.getDrawable(context, R.drawable.ic_draw_box),
            ConstantShapeIcon.LEFT_TOP
        )
        deleteIcon.iconListener = ResizeEvent()
        val doneIcon = Icon(
            ContextCompat.getDrawable(context, R.drawable.ic_draw_box),
            ConstantShapeIcon.RIGHT_TOP
        )
        doneIcon.iconListener = DoneIconEvent()
        val zoomIcon = Icon(
            ContextCompat.getDrawable(context, R.drawable.ic_draw_box),
            ConstantShapeIcon.RIGHT_BOTTOM
        )
        zoomIcon.iconListener = ZoomIconEvent()
        val rotateIcon = Icon(
            ContextCompat.getDrawable(context, R.drawable.ic_draw_box),
            ConstantShapeIcon.ROTATION
        )
        rotateIcon.iconListener = ZoomIconEvent()
        val flipIcon = Icon(
            ContextCompat.getDrawable(context, R.drawable.ic_draw_box),
            ConstantShapeIcon.LEFT_BOTTOM
        )
        flipIcon.iconListener = FlipIconEvent()
        icons.clear()
        icons.add(rotateIcon)
        icons.add(deleteIcon)
        icons.add(doneIcon)
        icons.add(zoomIcon)
        icons.add(flipIcon)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        currentShape?.let {
            transformSticker(it)
        }
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        if (changed) {
            stickerRect.left = left.toFloat()
            stickerRect.top = top.toFloat()
            stickerRect.right = right.toFloat()
            stickerRect.bottom = bottom.toFloat()
        }
    }

    override fun dispatchDraw(canvas: Canvas) {
        super.dispatchDraw(canvas)
        drawShape(canvas)
    }

    override fun onInterceptTouchEvent(ev: MotionEvent): Boolean {
        when (ev.action) {
            MotionEvent.ACTION_DOWN -> {
                downX = ev.x
                downY = ev.y
                return findCurrentIconTouched() != null || currentShape != null
            }
        }
        return super.onInterceptTouchEvent(ev)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        frameViewListener.onTouchEvent(event)
        when (event.actionMasked) {
            MotionEvent.ACTION_DOWN -> if (!onTouchDown(event)) {
                return false
            }
            MotionEvent.ACTION_POINTER_DOWN -> {
                oldDistance = calculateDistance(event)
                oldRotation = calculateRotation(event)
                midPoint = calculateMidPoint(event)
                if (currentShape != null && isInStickerArea(
                        currentShape!!, event.getX(1),
                        event.getY(1)
                    ) && findCurrentIconTouched() == null
                ) {
                    currentMode = ActionMode.ZOOM_WITH_TWO_FINGER
                }
            }
            MotionEvent.ACTION_MOVE -> {
                handleCurrentMode(event)
                invalidate()
            }
            MotionEvent.ACTION_UP -> onTouchUp(event)
            MotionEvent.ACTION_POINTER_UP -> {
                currentMode = ActionMode.NONE
            }
        }
        return true
    }

    private fun drawShape(canvas: Canvas) {
        currentShape?.draw(canvas, paint)
        if (currentShape != null) {
            getStickerPoints(currentShape, bitmapPoints)
            val x1 = bitmapPoints[0]
            val y1 = bitmapPoints[1]
            val x2 = bitmapPoints[2]
            val y2 = bitmapPoints[3]
            val x3 = bitmapPoints[4]
            val y3 = bitmapPoints[5]
            val x4 = bitmapPoints[6]
            val y4 = bitmapPoints[7]

            val x5 = x1 + ((x2 - x1) / 2)
            val y5 = y1 + ((y2 - y1) / 2)
            val rotation = calculateRotation(x4, y4, x3, y3)

            val point6 = calculateDistanceRotation(rotation, ConstantShapeIcon.DEFAULT_ROTATION_HEIGHT)
            val x6 = x5 + point6.x
            val y6 = y5 - point6.y

            //draw border
            canvas.drawLine(x1, y1, x2, y2, borderPaint)
            canvas.drawLine(x1, y1, x3, y3, borderPaint)
            canvas.drawLine(x2, y2, x4, y4, borderPaint)
            canvas.drawLine(x4, y4, x3, y3, borderPaint)
            canvas.drawLine(x5, y5, x6, y6, borderPaint)

            //draw icons
            for (i in icons.indices) {
                val icon = icons[i]
                when (icon.position) {
                    ConstantShapeIcon.LEFT_TOP -> configIconMatrix(icon, x1, y1, rotation)
                    ConstantShapeIcon.RIGHT_TOP -> configIconMatrix(icon, x2, y2, rotation)
                    ConstantShapeIcon.LEFT_BOTTOM -> configIconMatrix(icon, x3, y3, rotation)
                    ConstantShapeIcon.RIGHT_BOTTOM -> configIconMatrix(icon, x4, y4, rotation)
                    ConstantShapeIcon.ROTATION -> configIconMatrix(icon, x6, y6, rotation)
                }
                icon.draw(canvas, iconPaint)
            }
        }
    }

    private fun getStickerPoints(shape: BaseShape?, dst: FloatArray) {
        if (shape == null) {
            Arrays.fill(dst, 0f)
            return
        }
        shape.getBoundPoints(bounds)
        shape.getMappedPoints(dst, bounds)
    }

    private fun calculateDistance(event: MotionEvent?): Float {
        return if (event == null || event.pointerCount < 2) {
            0f
        } else calculateDistance(event.getX(0), event.getY(0), event.getX(1), event.getY(1))
    }

    private fun calculateDistance(x1: Float, y1: Float, x2: Float, y2: Float): Float {
        val x = x1 - x2.toDouble()
        val y = y1 - y2.toDouble()
        return sqrt(x * x + y * y).toFloat()
    }

    private fun calculateDistance(x: Float, y: Float): Float {
        return sqrt(x * x + y * y)
    }

    private fun calculateRotation(event: MotionEvent?): Float {
        return if (event == null || event.pointerCount < 2) {
            0f
        } else calculateRotation(event.getX(0), event.getY(0), event.getX(1), event.getY(1))
    }

    private fun calculateRotation(x1: Float, y1: Float, x2: Float, y2: Float): Float {
        val x = x1 - x2.toDouble()
        val y = y1 - y2.toDouble()
        val radians = atan2(y, x)
        return Math.toDegrees(radians).toFloat()
    }

    private fun configIconMatrix(icon: Icon, x: Float, y: Float, rotation: Float) {
        icon.x = x
        icon.y = y
        icon.matrix.reset()
        icon.matrix.postRotate(rotation, icon.width / 2.toFloat(), icon.height / 2.toFloat())
        icon.matrix.postTranslate(x - icon.width / 2, y - icon.height / 2)
    }

    private fun transformSticker(shape: BaseShape) {
        sizeMatrix.reset()
        val width = width.toFloat()
        val height = height.toFloat()
        val stickerWidth = shape.width.toFloat()
        val stickerHeight = shape.height.toFloat()
        //step 1
        val offsetX = (width - stickerWidth) / 2
        val offsetY = (height - stickerHeight) / 2
        sizeMatrix.postTranslate(offsetX, offsetY)

        //step 2
        val scaleFactor: Float = if (width < height) {
            width / stickerWidth
        } else {
            height / stickerHeight
        }
        sizeMatrix.postScale(scaleFactor / 2f, scaleFactor / 2f, width / 2f, height / 2f)
        shape.matrix.reset()
        shape.setMatrix(sizeMatrix)
        invalidate()
    }

    private fun findCurrentIconTouched(): Icon? {
        for (icon in icons) {
            val x = icon.x - downX
            val y = icon.y - downY
            val distancePow2 = x * x + y * y
            if (distancePow2 <= (icon.iconRadius + icon.iconRadius.toDouble()).pow(2.0)) {
                return icon
            }
        }
        return null
    }

    private fun onTouchDown(event: MotionEvent): Boolean {
        currentMode = ActionMode.DRAG
        downX = event.x
        downY = event.y
        midPoint = calculateMidPoint()
        oldDistance = calculateDistance(midPoint.x, midPoint.y, downX, downY)
        oldRotation = calculateRotation(midPoint.x, midPoint.y, downX, downY)
        currentIcon = findCurrentIconTouched()

        if (currentIcon != null) {
            currentMode = ActionMode.ICON
            currentIcon!!.onActionDown(this, event)
        }

        if (currentShape != null) {
            isTouchInsideSticker = currentShape!!.contains(downX, downY)
            downMatrix.set(BaseShape.getMatrix(currentShape!!))
        }

        if (currentIcon == null && !isTouchInsideSticker) {
            doneSticker(currentShape)
            return false
        }

        invalidate()
        return true
    }

    private fun handleCurrentMode(event: MotionEvent) {
        when (currentMode) {
            ActionMode.NONE, ActionMode.CLICK -> {
            }
            ActionMode.DRAG -> if (currentShape != null && isTouchInsideSticker) {
                moveMatrix.set(downMatrix)
                moveMatrix.postTranslate(event.x - downX, event.y - downY)
                currentShape!!.setMatrix(moveMatrix)
            }

            ActionMode.ZOOM_WITH_TWO_FINGER -> if (currentShape != null && isTouchInsideSticker) {
                val newDistance = calculateDistance(event)
                val newRotation = calculateRotation(event)
                moveMatrix.set(downMatrix)
                moveMatrix.postScale(
                    newDistance / oldDistance, newDistance / oldDistance, midPoint.x,
                    midPoint.y
                )
                moveMatrix.postRotate(newRotation - oldRotation, midPoint.x, midPoint.y)
                currentShape!!.setMatrix(moveMatrix)
            }
            ActionMode.ICON -> if (currentShape != null && currentIcon != null) {
                currentIcon!!.onActionMove(this, event)
            }
        }
    }

    private fun onTouchUp(event: MotionEvent) {
        if (currentMode == ActionMode.ICON && currentIcon != null && currentShape != null) {
            currentIcon!!.onActionUp(this, event)
        }
        if (currentMode == ActionMode.DRAG && abs(event.x - downX) < touchSlop && abs(event.y - downY) < touchSlop && currentShape != null) {
            if (!isTouchInsideSticker)
                frameViewListener.onClickStickerOutside(event.x, event.y)
            currentMode = ActionMode.CLICK
        }
        currentMode = ActionMode.NONE
    }

    private fun calculateMidPoint(event: MotionEvent?): PointF {
        if (event == null || event.pointerCount < 2) {
            midPoint[0f] = 0f
            return midPoint
        }
        val x = (event.getX(0) + event.getX(1)) / 2
        val y = (event.getY(0) + event.getY(1)) / 2
        midPoint[x] = y
        return midPoint
    }

    private fun calculateMidPoint(): PointF {
        if (currentShape == null) {
            midPoint[0f] = 0f
            return midPoint
        }
        currentShape?.getMappedCenterPoint(midPoint, point, tmp)
        return midPoint
    }

    private fun isInStickerArea(shape: BaseShape, downX: Float, downY: Float): Boolean {
        tmp[0] = downX
        tmp[1] = downY
        return shape.contains(tmp)
    }

    fun addSticker(shape: BaseShape): FrameView {
        return addSticker(shape, ConstantShape.CENTER)
    }

    private fun addSticker(shape: BaseShape, position: Int): FrameView {
        if (ViewCompat.isLaidOut(this)) {
            addStickerImmediately(shape, position)
        } else {
            post { addStickerImmediately(shape, position) }
        }
        return this
    }

    private fun addStickerImmediately(shape: BaseShape, position: Int) {
        setStickerPosition(shape, position)
        val scaleFactor: Float
        val widthScaleFactor: Float = width.toFloat() / shape.width
        val heightScaleFactor: Float = height.toFloat() / shape.height
        scaleFactor =
            if (widthScaleFactor > heightScaleFactor) heightScaleFactor else widthScaleFactor
        shape.matrix.postScale(
            scaleFactor / 2,
            scaleFactor / 2,
            width / 2.toFloat(),
            height / 2.toFloat()
        )
        currentShape = shape
        //stickers.add(sticker)
        invalidate()
    }

    private fun setStickerPosition(shape: BaseShape, position: Int) {
        val width = width.toFloat()
        val height = height.toFloat()
        var offsetX = width - shape.width
        var offsetY = height - shape.height
        when {
            position and ConstantShape.TOP > 0 -> offsetY /= 4f
            position and ConstantShape.BOTTOM > 0 -> offsetY *= 3f / 4f
            else -> offsetY /= 2f
        }
        when {
            position and ConstantShape.LEFT > 0 -> offsetX /= 4f
            position and ConstantShape.RIGHT > 0 -> offsetX *= 3f / 4f
            else -> offsetX /= 2f
        }
        shape.matrix.postTranslate(offsetX, offsetY)
    }

    private fun removeSticker(shape: BaseShape?) {
        if (shape == null)
            return
        currentShape = null
        this.visibility = View.GONE
        frameViewListener.onRemove()
    }

    private fun doneSticker(shape: BaseShape?) {
        if (shape == null)
            return
        currentShape = null
        this.visibility = View.GONE
        val obj = DrawObject(null, shape, DrawMode.SHAPE)
        frameViewListener.onDone(obj)
    }

    private fun zoomAndRotateSticker(shape: BaseShape?, event: MotionEvent) {
        if (shape == null)
            return
        var scaleX =
        moveMatrix.set(downMatrix)
        moveMatrix.postScale(
             downX / event.x, downY / event.x, midPoint.x,
            midPoint.y
        )
        moveMatrix.postRotate(newRotation - oldRotation, midPoint.x, midPoint.y)
        currentShape!!.setMatrix(moveMatrix)
        frameViewListener.onZoomAndRotate()
    }

    private fun resizeShape(shape: BaseShape?, event: MotionEvent) {
        if (shape == null)
            return
        val newDistanceX = calculateDistance(midPoint.x, midPoint.x, event.x, downX)
        val newDistanceY = abs(midPoint.y - event.y)
        val oldDistanceX = abs(midPoint.x - downX)
        val oldDistanceY = abs(midPoint.y - downY)
        moveMatrix.set(downMatrix)
        moveMatrix.postScale(newDistanceX / oldDistanceX, newDistanceY / oldDistanceY, midPoint.x,
            midPoint.y)
        currentShape!!.setMatrix(moveMatrix)
        frameViewListener.onShapeResize()
    }

    private fun flipSticker(shape: BaseShape?) {
        if (shape == null)
            return
        shape.getCenterPoint(midPoint)
        shape.matrix.preScale(-1f, 1f, midPoint.x, midPoint.y)
        shape.isFlippedHorizontally = !shape.isFlippedHorizontally
        invalidate()
        frameViewListener.onFlip()
    }

    fun remove() {
        removeSticker(currentShape)
    }

    fun done() {
        doneSticker(currentShape)
    }

    fun zoomAndRotate(event: MotionEvent) {
        zoomAndRotateSticker(currentShape, event)
    }

    fun flip() {
        flipSticker(currentShape)
    }

    fun resize(event: MotionEvent) {
        resizeShape(currentShape, event)
    }
}