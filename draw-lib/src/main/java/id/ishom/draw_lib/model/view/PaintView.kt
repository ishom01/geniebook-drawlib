package id.ishom.draw_lib.model.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.ViewConfiguration
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import id.ishom.draw_lib.constant.DrawMode
import id.ishom.draw_lib.listener.PaintViewListener
import id.ishom.draw_lib.model.DrawObject
import id.ishom.draw_lib.model.PathAndPaint
import id.ishom.draw_lib.model.shape.BaseShape
import id.ishom.draw_lib.model.shape.RectangleShape
import kotlin.math.abs

internal class PaintView (context: Context, private val paintViewListener: PaintViewListener) :
    FrameLayout(context) {

    private val drawColor = ResourcesCompat.getColor(resources, android.R.color.black, null)
    private var path = Path()
    private var mRect = RectF()
    private var isDrawing = false
    private val touchTolerance = ViewConfiguration.get(context).scaledTouchSlop
    private var drawMode = DrawMode.NULL
    private var obj: DrawObject? = null
    private var drawShape: BaseShape? = null

    private lateinit var extraCanvas: Canvas
    lateinit var extraBitmap: Bitmap

    val paint = Paint().apply {
        color = drawColor
        isAntiAlias = true
        isDither = true
        style = Paint.Style.STROKE
        strokeJoin = Paint.Join.ROUND
        strokeCap = Paint.Cap.ROUND
        strokeWidth = 10f
    }

    fun initCanvas() {
        extraBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        extraCanvas = Canvas(extraBitmap)
        extraCanvas.drawColor(ContextCompat.getColor(context, android.R.color.white))
        invalidate()
    }

    override fun onSizeChanged(width: Int, height: Int, oldWidth: Int, oldHeight: Int) {
        super.onSizeChanged(width, height, oldWidth, oldHeight)
        initCanvas()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.drawBitmap(extraBitmap, 0f, 0f, null)
    }

    fun drawPath(pathAndPaint: PathAndPaint) {
        extraCanvas.drawPath(pathAndPaint.path, pathAndPaint.paint)
        invalidate()
    }

    fun drawSticker(shape: BaseShape) {
        shape.draw(extraCanvas, paint)
        invalidate()
    }

    fun setDrawMode(drawMode: DrawMode) {
        this.drawMode = drawMode
    }

    //region touch events
    private val gestureDetector =
        GestureDetector(context, object : GestureDetector.SimpleOnGestureListener() {
            override fun onSingleTapConfirmed(event: MotionEvent?): Boolean {
                event?.let {
                    paintViewListener.onClick(it.x, it.y)
                }
                return super.onSingleTapConfirmed(event)
            }
        })

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        paintViewListener.onTouchEvent(event)
        mRect.right = event.x
        mRect.bottom = event.y
        when (event.action) {
            MotionEvent.ACTION_DOWN -> touchStart()
            MotionEvent.ACTION_MOVE -> touchMove()
            MotionEvent.ACTION_UP -> touchUp()
        }
        gestureDetector.onTouchEvent(event)
        return true
    }

    private fun touchStart() {
        path.reset()
        path.moveTo(mRect.right, mRect.bottom)
        mRect.left = mRect.right
        mRect.top = mRect.bottom
    }

    private fun touchMove() {
        val dx = abs(mRect.right - mRect.left)
        val dy = abs(mRect.bottom - mRect.right)
        if (dx >= touchTolerance || dy >= touchTolerance) {
            isDrawing = true
            when (drawMode) {
                DrawMode.LINE -> {
                    path.quadTo(
                        mRect.left,
                        mRect.top,
                        (mRect.right + mRect.left) / 2,
                        (mRect.bottom + mRect.top) / 2
                    )
                    mRect.left = mRect.right
                    mRect.top = mRect.bottom
                    extraCanvas.drawPath(path, paint)
                }
                DrawMode.SHAPE -> {
                    if (drawShape == null) {
                        drawShape = RectangleShape(mRect)
                    }
                    drawShape?.draw(extraCanvas, paint)
                }
                DrawMode.NULL -> isDrawing = false
            }
        }
        invalidate()
    }

    private fun touchUp() {
        if (isDrawing) {
            when (drawMode) {
                DrawMode.LINE -> {
                    obj = DrawObject(PathAndPaint(Path(path), Paint(paint)), null, DrawMode.LINE)
                }
                DrawMode.SHAPE -> {
                    obj = DrawObject(null, drawShape, DrawMode.SHAPE)
                    paintViewListener.onFinishDrawObj(obj!!)
                }
                DrawMode.NULL -> obj = null
            }
            obj?.apply {
                paintViewListener.onTouchUp(this)
            }
        }
        invalidate()
        path.reset()
        drawShape = null
        isDrawing = false
    }
    //endregion
}