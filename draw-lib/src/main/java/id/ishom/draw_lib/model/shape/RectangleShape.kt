package id.ishom.draw_lib.model.shape

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.RectF
import android.graphics.drawable.Drawable
import kotlin.math.abs
import kotlin.math.roundToInt

internal class RectangleShape(private val mRect: RectF): BaseShape() {

    final override val width: Int
        get() = abs(mRect.left - mRect.right).roundToInt()

    final override val height: Int
        get() = abs(mRect.bottom - mRect.top).roundToInt()

    override val drawable: Drawable?
        get() = null

    private val realBounds = Rect(0, 0, width, height)

    override fun draw(canvas: Canvas, paint: Paint) {
        canvas.save()
        canvas.concat(matrix)
        canvas.drawRect(mRect, paint)
        canvas.restore()
    }

    override fun setDrawable(drawable: Drawable): BaseShape {
        return this
    }

    override fun setAlpha(alpha: Int): BaseShape {
        return this
    }
}