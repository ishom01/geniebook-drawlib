package id.ishom.draw_lib.model.shape

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.drawable.Drawable

internal open class DrawableShape(override var drawable: Drawable): BaseShape() {
    private val realBounds: Rect

    final override val width: Int
        get() = drawable.intrinsicWidth

    final override val height: Int
        get() = drawable.intrinsicHeight

    init {
        realBounds = Rect(0, 0, width, height)
    }

    override fun setDrawable(drawable: Drawable): DrawableShape {
        this.drawable = drawable
        return this
    }

    override fun setAlpha(alpha: Int): BaseShape {
        drawable.alpha = alpha
        return this
    }

    override fun draw(canvas: Canvas, paint: Paint) {
        canvas.save()
        canvas.concat(matrix)
        drawable.bounds = realBounds
        drawable.draw(canvas)
        canvas.restore()
    }
}