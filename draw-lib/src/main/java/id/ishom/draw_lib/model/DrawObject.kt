package id.ishom.draw_lib.model

import id.ishom.draw_lib.constant.DrawMode
import id.ishom.draw_lib.model.shape.BaseShape

internal data class DrawObject(
    val pathAndPaint: PathAndPaint?,
    val shape: BaseShape?,
    val drawMode: DrawMode
)