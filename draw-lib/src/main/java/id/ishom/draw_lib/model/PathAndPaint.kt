package id.ishom.draw_lib.model

import android.graphics.Paint
import android.graphics.Path

internal data class PathAndPaint(
    val path: Path,
    val paint: Paint
)