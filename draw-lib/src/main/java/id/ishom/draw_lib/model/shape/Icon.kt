package id.ishom.draw_lib.model.shape

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.drawable.Drawable
import android.view.MotionEvent
import id.ishom.draw_lib.constant.ConstantShapeIcon
import id.ishom.draw_lib.listener.IconListener
import id.ishom.draw_lib.model.view.FrameView

internal class Icon(drawable: Drawable?, gravity: Int): DrawableShape(drawable!!), IconListener {
    var iconRadius = ConstantShapeIcon.DEFAULT_ICON_RADIUS
    var x = 0f
    var y = 0f

    var position = ConstantShapeIcon.LEFT_TOP
    var iconListener: IconListener? = null

    init {
        position = gravity
    }

    override fun onActionDown(frameView: FrameView?, event: MotionEvent?) {
        iconListener?.onActionDown(frameView, event)
    }

    override fun onActionMove(frameView: FrameView, event: MotionEvent) {
        iconListener?.onActionMove(frameView, event)
    }

    override fun onActionUp(frameView: FrameView, event: MotionEvent?) {
        iconListener?.onActionUp(frameView, event)
    }

    override fun draw(canvas: Canvas, paint: Paint) {
        canvas.drawCircle(x, y, iconRadius, paint)
        super.draw(canvas, paint)
    }

    override fun setDrawable(drawable: Drawable): DrawableShape {
        this.drawable = drawable
        return this
    }

    override fun setAlpha(alpha: Int): BaseShape {
        drawable.alpha = alpha
        return this
    }
}