package id.ishom.draw_lib

import android.graphics.PointF
import android.util.Log
import kotlin.math.cos
import kotlin.math.sin

fun calculateDistanceRotation(rotation: Float, distance: Float): PointF {
    val x = distance * (sin(Math.toRadians(rotation.toDouble())))
    val y = distance * (cos(Math.toRadians(rotation.toDouble())))
    Log.e("Canvas rotation", " sin cos $rotation, $x, $y")
    return PointF(x.toFloat(), y.toFloat())
}