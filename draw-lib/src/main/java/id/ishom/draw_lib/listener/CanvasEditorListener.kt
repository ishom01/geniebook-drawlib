package id.ishom.draw_lib.listener

import android.view.MotionEvent

interface CanvasEditorListener {
    fun onEnableUndo(isEnable: Boolean)
    fun onEnableRedo(isEnable: Boolean)
    fun onTouchEvent(event: MotionEvent) {}

    fun onShapeActive() {}
    fun onShapeRemove() {}
    fun onShapeDone() {}
    fun onShapeZoomAndRotate() {}
    fun onShapeResize() {}
    fun onShapeFlip() {}
}