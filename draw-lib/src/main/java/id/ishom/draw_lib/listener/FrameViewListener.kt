package id.ishom.draw_lib.listener

import android.view.MotionEvent
import id.ishom.draw_lib.model.DrawObject

internal interface FrameViewListener {
    fun onRemove()
    fun onDone(obj: DrawObject)
    fun onZoomAndRotate()
    fun onShapeResize()
    fun onFlip()
    fun onClickStickerOutside(x: Float, y: Float)
    fun onTouchEvent(event: MotionEvent)
}