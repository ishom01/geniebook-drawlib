package id.ishom.draw_lib.listener

import android.graphics.RectF
import android.view.MotionEvent
import id.ishom.draw_lib.model.DrawObject

internal interface PaintViewListener {
    fun onTouchUp(obj: DrawObject)
    fun onClick(x: Float, y: Float)
    fun onTouchEvent(event: MotionEvent)
    fun onFinishDrawObj(obj: DrawObject)
}