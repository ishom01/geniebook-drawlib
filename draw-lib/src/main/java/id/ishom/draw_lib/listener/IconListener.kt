package id.ishom.draw_lib.listener

import android.view.MotionEvent
import id.ishom.draw_lib.model.view.FrameView

internal interface IconListener {
    fun onActionDown(frameView: FrameView?, event: MotionEvent?)
    fun onActionMove(frameView: FrameView, event: MotionEvent)
    fun onActionUp(frameView: FrameView, event: MotionEvent?)
}