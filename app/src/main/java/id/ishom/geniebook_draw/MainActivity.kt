package id.ishom.geniebook_draw

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.content.ContextCompat
import id.ishom.draw_lib.constant.DrawMode
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var drawMode = arrayOf(
        "NULL",
        "LINE",
        "SHAPE",
        "Drawbale"
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initView()
        initEvent()
    }

    private fun initEvent() {
        sp_draw_mode.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
                if (position == 3) {
                    ContextCompat.getDrawable(this@MainActivity, R.drawable.ic_sample_drawable)?.let {
                        layout.addDrawableSticker(it)
                    }
                    return
                }
                layout.drawMode = DrawMode.values()[position]
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        btn_remove.setOnClickListener {
            layout.removeAll()
        }
    }

    private fun initView() {
        sp_draw_mode.adapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, drawMode)
    }
}